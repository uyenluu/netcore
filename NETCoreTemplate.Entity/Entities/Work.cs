﻿using System;
using System.Collections.Generic;

#nullable disable

namespace NETCoreTemplate.Entity.Entities
{
    public partial class Work
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
